use std::env;
use std::ffi::OsStr;
use std::io::{self, BufRead, BufReader, Write};
use std::os::unix::ffi::OsStrExt;
use std::path::{Path, PathBuf};

const HELP: &str = "
USAGE: [files |] mve [files]

    The editor is picked by EDITOR or VISUAL environmental variables.
    The items for editing are combined from both STDIN and ARGS.

HINTS:
    A -> B      A was renamed to B
    A -! B      can't rename, because B exists
    A -? B      can't access B
    A -! <tmp>  can't create a temp for A
    ?: A        A does not exist
    N =/= M     unique input and output items quantity missmatch
    <cancel>    editing was not successful
";

const BUFSIZE: usize = 64 * 1024;
const SUFFIX: [char; 8] = ['☰', '☱', '☲', '☳', '☴', '☵', '☶', '☷'];

fn abort(msg: &str) -> ! {
    eprintln!("{}", msg);
    std::process::exit(1);
}

fn sample<T: Copy + Default, const Q: usize>(items: &[T]) -> [T; Q] {
    let mut result = [T::default(); Q];
    for i in result.iter_mut() {
        let idx = fastrand::usize(..items.len());
        *i = items[idx];
    }
    result
}

fn main() {
    let mut input: Vec<PathBuf> = env::args_os().skip(1).map(PathBuf::from).collect();

    if !atty::is(atty::Stream::Stdin) {
        let mut reader = BufReader::with_capacity(BUFSIZE, io::stdin());
        let mut buf: Vec<u8> = Vec::with_capacity(1024);

        while let Ok(num) = reader.read_until(b'\n', &mut buf) {
            if num == 0 {
                break;
            }
            let os_str: &OsStr = OsStrExt::from_bytes(&buf[..&buf.len() - 1]);
            input.push(PathBuf::from(os_str));
            buf.clear();
        }
    }

    let mut infiles: Vec<PathBuf> = input
        .into_iter()
        .filter(|i| i.exists() || abort(&format!("?: {}", i.display())))
        .map(|i| i.strip_prefix("./").map(PathBuf::from).unwrap_or(i))
        .collect();

    if infiles.is_empty() {
        eprint!("{}", HELP);
        std::process::exit(1);
    }

    infiles.dedup();

    let to_edit = infiles
        .iter()
        .map(|f| f.to_string_lossy().replace('\n', "↵"))
        .collect::<Vec<_>>();

    let outfiles = match edit::edit(to_edit.join("\n")) {
        Ok(out) => {
            let mut out_lines = out
                .lines()
                .map(|l| PathBuf::from(l.trim()))
                .collect::<Vec<_>>();
            out_lines.dedup();
            out_lines
        }
        Err(_) => abort("<cancel>"),
    };

    if infiles.len() != outfiles.len() {
        abort(&format!("{} in =/= {} out", infiles.len(), outfiles.len()));
    }

    let tasks: Vec<Pair> = infiles
        .into_iter()
        .zip(outfiles.into_iter())
        .filter(|(s, d)| s != d)
        .map(|(s, d)| Pair(s, d))
        .collect();

    if let Err(e) = run_tasks(tasks) {
        abort(&e.to_string());
    }
}

struct Pair(PathBuf, PathBuf);

impl Pair {
    fn unpack(&self) -> (&Path, &Path) {
        (self.0.as_path(), self.1.as_path())
    }
}

struct Tasks(Vec<Pair>);

impl Tasks {
    fn pop(&mut self) -> Option<Pair> {
        self.0.pop()
    }

    fn sources(&self) -> Vec<&Path> {
        self.0.iter().map(|p| p.0.as_path()).collect()
    }
}

struct Conflicts(Vec<Pair>);

impl Conflicts {
    fn push(&mut self, pair: Pair) {
        self.0.push(pair);
    }

    fn pop(&mut self) -> Option<Pair> {
        self.0.pop()
    }

    fn sources(&self) -> Vec<&Path> {
        self.0.iter().map(|p| p.0.as_path()).collect()
    }
}

struct Temp {
    source: PathBuf,
    temp: PathBuf,
    dest: PathBuf,
}

impl Temp {
    pub fn new(pair: Pair) -> io::Result<Self> {
        let (s, _) = pair.unpack();
        let parent = s.parent().ok_or_else(|| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("No parent for: '{}'", s.display()),
            )
        })?;

        for _ in 1..=3 {
            let rnd: String = sample::<char, 8>(&SUFFIX).into_iter().collect();
            let temp = parent.join(rnd);
            if temp.exists() {
                continue;
            } else {
                std::fs::rename(s, &temp)?;
                let source = pair.0;
                let dest = pair.1;
                return Ok(Self { source, temp, dest });
            }
        }

        Err(io::Error::new(
            io::ErrorKind::Other,
            format!("{} -! <tmp>", s.display()),
        ))
    }

    fn release(&self, logger: &mut impl Write) -> io::Result<()> {
        rename(
            &self.temp,
            &self.dest,
            logger,
            Some(&format!(
                "{} -> {}",
                self.source.display(),
                self.dest.display()
            )),
        )?;

        Ok(())
    }
}

fn puts(s: &str, logger: &mut impl Write) -> io::Result<()> {
    logger.write_all(s.as_bytes())?;
    logger.write_all(b"\n")?;
    Ok(())
}

fn run_tasks(tasks: Vec<Pair>) -> io::Result<()> {
    let mut tasks = Tasks(tasks);
    let mut conflicts = Conflicts(Vec::new());
    let mut temps: Vec<Temp> = Vec::new();
    let mut stderr = io::stderr().lock();

    while let Some(pair) = tasks.pop() {
        let (s, d) = pair.unpack();

        if tasks.sources().contains(&d) {
            conflicts.push(pair);
        } else if d.exists() {
            if conflicts.sources().contains(&d) {
                temps.push(Temp::new(pair)?);
            } else {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    format!("{} -! {}", s.display(), d.display()),
                ));
            }
        } else {
            rename(s, d, &mut stderr, None)?;
        }
    }

    while let Some(pair) = conflicts.pop() {
        let (s, d) = pair.unpack();
        rename(s, d, &mut stderr, None)?;
    }

    for t in temps {
        t.release(&mut stderr)?
    }

    Ok(())
}

fn rename(s: &Path, d: &Path, logger: &mut impl Write, msg: Option<&str>) -> io::Result<()> {
    match std::fs::rename(s, d) {
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            return Err(io::Error::new(
                io::ErrorKind::NotFound,
                format!("{} -? {}", s.display(), d.display()),
            ));
        }
        Err(e) => return Err(e),
        _ => (),
    }
    match msg {
        Some(msg) => puts(msg, logger)?,
        None => puts(&format!("{} -> {}", s.display(), d.display()), logger)?,
    }
    Ok(())
}
